## Git

### Multiple accounts
Joseph Smith's "first vision" had multiple accounts, you can too.

[Quick guide](https://code.tutsplus.com/tutorials/quick-tip-how-to-work-with-github-and-multiple-accounts--net-22574) for working with multiple accounts.

## Ideas
The [ideas](https://gitlab.com/tapir-riders/self/wikis/ideas) wiki is a dump of ideas limited to one line of explanation & tags.

Create an [issue](https://gitlab.com/tapir-riders/self/issues) if an idea needs more detail, discussion, voting, or to be assigned to someone.


