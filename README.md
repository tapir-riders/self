See--yonder is matter unorganized. Go ye down and organize it into a world like unto the worlds that we have heretofore formed.

(Still working on this)

# About

[**Tapir-riders**](https://gitlab.com/tapir-riders) is a GitLab group that exists to make it easier to collaborate on code, data, and content relating to faith transition and mormonism (Gitlab Docs on [groups](https://docs.gitlab.com/ee/user/group/))

### Open

Access to information is vital to freedom.

You can lead a tapir to water but you can't make it drink. Having your world shattered is not fun, and I would not force that on anyone. However, information needs to be easily accessible so people can make informed decisions.

> "It is error only, and not truth, that shrinks from inquiry."  *- [Thomas Paine](https://en.wikiquote.org/wiki/Talk:Thomas_Paine)*

> If we have the truth, it cannot be harmed by investigation. If we have not the truth, it ought to be harmed. *- J. Reuben Clark*

Don't hide your light under a bushel. Let it shine - for what has the truth got to fear?

Fear is the opposite of faith.

### Privacy

This group exists primarily to collaborate on publicly accessible data. The openness that we promote is to encourage accountability and protect people.

[MormonLeaks.io](https://MormonLeaks.io) is better equipped to handle sensitive or private data.

## Projects

Projects can contain anything from data to graphics to useful tools. 

[Self](https://gitlab.com/tapir-riders/self/) and [resource-list](https://gitlab.com/tapir-riders/resource-list) are meta. Some may highlight changes in text over time via git diff.

Naturally, many of us want to at least try to have lives so I would encourage doing "minimum viable products" as much as possible. But I think a little collaboration can go a long way.

### Subgroups

- **Analysis**: Data, statistics, and other analysis.

  > See--yonder is matter unorganized. Go ye down and organize it into a world like unto the worlds that we have heretofore formed. Call your labors the first day, and bring me word.

- **Dev**: Code

  > Go down again. Gather the waters together and cause the dry land to appear. The great waters call ye Seas, and the dry land call ye Earth. Form mountains and hills, great rivers and small streams, to beautify and give variety to the face of the earth.
  >
  > Return again to the earth that you have organized. Divide the light from the darkness. Call the light Day, and the darkness Night. Cause the lights in the firmament to appear--the greater light to rule the day, and the lesser light to rule the night. Cause the stars also to appear and give light to the earth, the same as with other worlds heretofore created.

- **Content** to place upon the land.

  > Return again. Place seeds of all kinds in the earth that they may spring forth as grass, flowers, shrubbery, trees, and all manner of vegetation, each bearing seed in itself after its own kind, as on the worlds we have heretofore created.
  >
  > Now that the earth is formed, divided, and beautified, and vegetation is growing thereon, return and place beasts upon the land: the elephant, the lion, the tiger, the bear, the horse, and all other kinds of animals--fowls in the air in all their varieties, fishes of all kinds in the waters, and insects and all manner of animal life upon the earth.



### Disclaimers

Flaming tapir-riding snoo [by u/zelph_esteem](https://www.reddit.com/r/exmormon/comments/70qf4j/i_made_another_snoo_i_didnt_want_to_but_an_angel/).

> "The cherubim and flaming sword were not evidence of God’s anger and 
> rejection. Rather, they were evidence of his benevolence and love." - *[Ensign, 1993](https://www.lds.org/ensign/1993/12/closed-doors-and-open-windows?lang=eng)*

### Contact

[SCMC](https://en.wikipedia.org/wiki/Strengthening_Church_Members_Committee) feel free to call 888.746.6337.